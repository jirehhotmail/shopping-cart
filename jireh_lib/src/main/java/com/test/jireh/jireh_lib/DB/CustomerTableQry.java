package com.test.jireh.jireh_lib.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.jireh.jireh_lib.Model.Customer;

/**
 * Created by Muthamizhan C on 27-07-2017.
 */

public class CustomerTableQry {


    private static final String TABLE_CUSTOMER = "CUSTOMER";
    private static final String COLUMN_SNO = "S_NO";
    private static final String COLUMN_FIRST_NAME = "FIRST_NAME";
    private static final String COLUMN_LAST_NAME = "LAST_NAME";
    private static final String COLUMN_EMAIL_ID = "EMAIL_ID";
    private static final String COLUMN_MOBILE_NO = "MOBILE_NO";
    private static final String COLUMN_PASSWORD = "PASSWORD";
    private static final String COLUMN_ADDRESS1 = "ADDRESS1";
    private static final String COLUMN_ADDRESS2 = "ADDRESS2";
    private static final String COLUMN_CITY = "CITY";
    private static final String COLUMN_PINCODE = "PINCODE";
    private static final String COLUMN_STATE = "STATE";
    private static final String COLUMN_COUNTRY = "COUNTRY";


    public void insertCustomer(Customer customers, Context context) {
        String sql = "INSERT INTO " + TABLE_CUSTOMER + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqlite = database.getWritableDatabase();
        SQLiteStatement statement = sqlite.compileStatement(sql);
        sqlite.beginTransaction();
        statement.clearBindings();
        statement.bindString(2, customers.getFirst_name());
        statement.bindString(3, customers.getLast_name());
        statement.bindString(4, customers.getEmail_id());
        statement.bindString(5, customers.getMobile_no());
        statement.bindString(6, customers.getPassword());
        statement.bindString(7, customers.getAddress1());
        statement.bindString(8, customers.getAddress2());
        statement.bindString(9, customers.getCity());
        statement.bindString(10,customers.getPincode());
        statement.bindString(11, customers.getState());
        statement.bindString(12, customers.getCountry());
        statement.execute();
        sqlite.setTransactionSuccessful();
        sqlite.endTransaction();

    }

    public int userCheck(String emailid, String password, Context context) {
        String sql = " SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + COLUMN_EMAIL_ID + " = '" + emailid + "'" + " and " + COLUMN_PASSWORD + " = '" + password + "'";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        int customerId = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                customerId = cursor.getInt(0);
            }
            cursor.close();
        }
        database.close();
        return customerId;
    }

    public Customer getCustomerData(int customerId, Context context) {
        String sql = " SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + COLUMN_SNO + " = " + customerId;
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        Customer customer = new Customer();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                customer.setFirst_name(cursor.getString(cursor.getColumnIndex(COLUMN_FIRST_NAME)));
                customer.setLast_name(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_NAME)));
                customer.setEmail_id(cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL_ID)));
                customer.setMobile_no(cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE_NO)));
                customer.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD)));
                customer.setAddress1(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS1)));
                customer.setAddress2(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS2)));
                customer.setCity(cursor.getString(cursor.getColumnIndex(COLUMN_CITY)));
                customer.setPincode(cursor.getString(cursor.getColumnIndex(COLUMN_PINCODE)));
                customer.setState(cursor.getString(cursor.getColumnIndex(COLUMN_STATE)));
                customer.setCountry(cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY)));
            }
            cursor.close();
        }
        database.close();
        return customer;
    }

    public int updateCustomer(Customer customer, int customerId, Context context) {
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqlite = database.getWritableDatabase();
        ContentValues contentValue = new ContentValues();
        contentValue.put(COLUMN_FIRST_NAME, customer.getFirst_name());
        contentValue.put(COLUMN_EMAIL_ID, customer.getEmail_id());
        contentValue.put(COLUMN_MOBILE_NO, customer.getMobile_no());
        return sqlite.update(TABLE_CUSTOMER, contentValue, COLUMN_SNO + " = ? ", new String[]{String.valueOf(customerId)});

    }
}
