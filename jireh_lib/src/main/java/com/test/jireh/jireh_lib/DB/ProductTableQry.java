package com.test.jireh.jireh_lib.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.jireh.jireh_lib.Model.Product;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 19-07-2017.
 */

public class ProductTableQry {

    private static final String TABLE_PRODUCT = "PRODUCT";

    private static final String COLUMN_PRODUCT_ID = "PRODUCT_ID";
    private static final String COLUMN_PRODUCT_NAME = "PRODUCT_NAME";
    private static final String COLUMN_PRODUCT_IMAGE = "PRODUCT_IMG";
    private static final String COLUMN_PRODUCT_DIMAGE = "PRODUCT_DIMG";
    private static final String COLUMN_PRODUCT_PRICE = "PRODUCT_PRICE";
    private static final String COLUMN_PRODUCT_DPRICE = "PRODUCT_DISCOUNT_PRICE";
    private static final String COLUMN_PRODUCT_CATID = "CATEGORY_ID";
    private static final String COLUMN_STATUS = "PRODUCT_STATUS";
    private static final String COLUMN_PRODUCT_SIMAGE = "PRODUCT_SIMG";
    private static final String COLUMN_PRODUCT_HIMAGE = "PRODUCT_HIMG";
    private static final String COLUMN_PRODUCT_VIMAGE = "PRODUCT_VIMG";


    public void insertProductDB(ArrayList<Product> products, Context context) {
        String sql = "INSERT OR REPLACE INTO " + TABLE_PRODUCT + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();

        for (int idx = 0; idx < products.size(); idx++) {
            statement.clearBindings();
            statement.bindLong(2, products.get(idx).getProduct_id());
            statement.bindString(3, products.get(idx).getProduct_name());
            statement.bindDouble(4, products.get(idx).getProduct_price());
            statement.bindDouble(5, products.get(idx).getProduct_dprice());
            statement.bindLong(6, products.get(idx).getProduct_catid());
            statement.bindString(7, products.get(idx).getProduct_status());
            statement.bindLong(8, products.get(idx).getProduct_image());
            statement.bindString(9, products.get(idx).getProduct_dimage());
            statement.bindString(10, products.get(idx).getProduct_simage());
            statement.bindString(11, products.get(idx).getProduct_himage());
            statement.bindString(12, products.get(idx).getProduct_vimage());
            statement.execute();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public ArrayList<Product> getAllProducts(Context context) {
        String sql = "SELECT * FROM " + TABLE_PRODUCT + ";";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<Product> allProducts = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {
                Product product = new Product();
                product.setProduct_id(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_ID)));
                product.setProduct_catid(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_CATID)));
                product.setProduct_price(cur.getDouble(cur.getColumnIndex(COLUMN_PRODUCT_PRICE)));
                product.setProduct_dprice(cur.getDouble(cur.getColumnIndex(COLUMN_PRODUCT_DPRICE)));
                product.setProduct_name(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_NAME)));
                product.setProduct_image(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_IMAGE)));
                product.setProduct_dimage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_DIMAGE)));
                product.setProduct_simage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_SIMAGE)));
                product.setProduct_himage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_HIMAGE)));
                product.setProduct_vimage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_VIMAGE)));
                product.setProduct_status(cur.getString(cur.getColumnIndex(COLUMN_STATUS)));
                allProducts.add(product);

            } while (cur.moveToNext());
        }
        return allProducts;
    }

    public ArrayList<Product> searchProduct(String productName, Context context) {
        String sql = "SELECT * FROM " + TABLE_PRODUCT + " WHERE " + COLUMN_PRODUCT_NAME + " LIKE  '%" + productName + "%'";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<Product> allProducts = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {
                Product product = new Product();
                product.setProduct_id(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_ID)));
                product.setProduct_catid(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_CATID)));
                product.setProduct_price(cur.getDouble(cur.getColumnIndex(COLUMN_PRODUCT_PRICE)));
                product.setProduct_dprice(cur.getDouble(cur.getColumnIndex(COLUMN_PRODUCT_DPRICE)));
                product.setProduct_name(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_NAME)));
                product.setProduct_image(cur.getInt(cur.getColumnIndex(COLUMN_PRODUCT_IMAGE)));
                product.setProduct_dimage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_DIMAGE)));
                product.setProduct_status(cur.getString(cur.getColumnIndex(COLUMN_STATUS)));
                product.setProduct_simage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_SIMAGE)));
                product.setProduct_himage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_HIMAGE)));
                product.setProduct_vimage(cur.getString(cur.getColumnIndex(COLUMN_PRODUCT_VIMAGE)));
                allProducts.add(product);

            } while (cur.moveToNext());
        }
        return allProducts;
    }
}
