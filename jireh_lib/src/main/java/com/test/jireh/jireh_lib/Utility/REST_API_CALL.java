package com.test.jireh.jireh_lib.Utility;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.test.jireh.jireh_lib.App.Singleton;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class REST_API_CALL {

    private static final String TAG = "VOLLEY";
    private static final String tag_json_obj_req = "json_object_req";
    private static final int MY_SOCKET_TIMEOUT_MS = 5000;

    public interface OnRestApiCallBack {
        public void OnRestResponse(boolean result, JSONObject jsonObject);
    }

    public void post(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void post(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void post(String url, JSONArray params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONArray jsonObjectParms=null;
        if(jsonObjectParms !=null)
        {
            jsonObjectParms = params;
        }



        JsonPostArrayRequest jsonObjReq = new JsonPostArrayRequest(
                url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        },jsonObjectParms) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void get(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms=null;
        if(params ==null)
        {
            jsonObjectParms =null;
        }
        else {
            jsonObjectParms =new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void get(String url,  final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void put(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.PUT, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void put(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.PUT, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void delete(String url, Map<String, String> params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = null;
        if (params != null) {
            jsonObjectParms = new JSONObject(params);
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.DELETE, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }
    public void delete(String url, JSONObject params, final OnRestApiCallBack onRestApiCallBack, final Request.Priority priority) {
// Convert mapForm to jsonObject
        JSONObject jsonObjectParms = params;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.DELETE, url, jsonObjectParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        VolleyLog.d(TAG, response.toString());
                        onRestApiCallBack.OnRestResponse(true, response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, error.getMessage());
                onRestApiCallBack.OnRestResponse(false, null);
            }


        }) {
            public Priority getPriority() {
                return priority;
            }

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Singleton.getInstance().getRequestQueue();

        Singleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj_req);

    }

}
