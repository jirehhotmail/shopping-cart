package com.test.jireh.jireh_lib.Model;

import java.io.Serializable;

/**
 * Created by Muthamizhan C on 7/14/2017.
 */

public class Product implements Serializable {
    int product_id;
    String product_name;
    int product_catid;
    int product_image;
    String product_qty;
    double product_price;
    double product_dprice;
    String product_status;
    String product_dimage;
    String product_simage;
    String product_himage;
    String product_vimage;

    public Product() {

    }

    public Product(int product_id, String product_name, int product_catid, int product_image, double product_price,
                   double pdt_dprice, String pdt_status, String pdt_dimage, String pdt_simage, String pdt_himage, String pdt_vimage) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_catid = product_catid;
        this.product_image = product_image;
        this.product_price = product_price;
        this.product_dprice = pdt_dprice;
        this.product_status = pdt_status;
        this.product_dimage = pdt_dimage;
        this.product_simage = pdt_simage;
        this.product_himage = pdt_himage;
        this.product_vimage = pdt_vimage;

    }

    public String getProduct_simage() {
        return product_simage;
    }

    public void setProduct_simage(String product_simage) {
        this.product_simage = product_simage;
    }

    public String getProduct_himage() {
        return product_himage;
    }

    public void setProduct_himage(String product_himage) {
        this.product_himage = product_himage;
    }

    public String getProduct_vimage() {
        return product_vimage;
    }

    public void setProduct_vimage(String product_vimage) {
        this.product_vimage = product_vimage;
    }

    public String getProduct_dimage() {
        return product_dimage;
    }

    public void setProduct_dimage(String product_dimage) {
        this.product_dimage = product_dimage;
    }

    public String getProduct_status() {
        return product_status;
    }

    public void setProduct_status(String product_status) {
        this.product_status = product_status;
    }

    public int getProduct_catid() {
        return product_catid;
    }

    public void setProduct_catid(int product_catid) {
        this.product_catid = product_catid;
    }


    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(String product_qty) {
        this.product_qty = product_qty;
    }

    public double getProduct_price() {
        return product_price;
    }

    public void setProduct_price(double product_price) {
        this.product_price = product_price;
    }

    public double getProduct_dprice() {
        return product_dprice;
    }

    public void setProduct_dprice(double product_dprice) {
        this.product_dprice = product_dprice;
    }

}
