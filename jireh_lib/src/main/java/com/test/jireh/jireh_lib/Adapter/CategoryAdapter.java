package com.test.jireh.jireh_lib.Adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.test.jireh.jireh_lib.Model.Category;
import com.test.jireh.jireh_lib.R;


/**
 * Created by Muthamizhan C on 7/14/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {
    List<Category> categoryList;
    CategoryAdapter.OnItemClickListener listener;

    public CategoryAdapter(List<Category> categoryList,CategoryAdapter.OnItemClickListener listener) {
        this.categoryList = categoryList;
        this.listener =listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView category_image;
        TextView category_name;
        LinearLayout category_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            category_image = (ImageView) itemView.findViewById(R.id.category_image);
            category_name = (TextView) itemView.findViewById(R.id.category_name);
            category_layout = (LinearLayout) itemView.findViewById(R.id.category_layout);
        }
    }

    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.MyViewHolder holder, final int position) {
        Category category= categoryList.get(position);
        holder.category_name.setText(category.getCategory_name());
        holder.category_image.setImageResource(category.getCategory_image());
        holder.category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(R.id.category_layout,position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

 public  interface OnItemClickListener
    {
         void onItemClick(int var1,int var2);
    }
}
