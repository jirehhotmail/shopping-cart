package com.test.jireh.jireh_lib.Interface;

/**
 * Created by Muthamizhan C on 26-07-2017.
 */

public class CartTrigger {
    private static CartTrigger minstance = null;
    private cartChangeListener listener;


    public CartTrigger() {

    }

    public static CartTrigger getInstance() {
        if (minstance == null) {
            minstance = new CartTrigger();
        }
        return minstance;
    }

    public void triggerCartChange() {
        listener.callback();
    }

    public void setCustomObjectReference(cartChangeListener listener) {
        this.listener = listener;
    }

    public interface cartChangeListener {
        public void callback();
    }
}
