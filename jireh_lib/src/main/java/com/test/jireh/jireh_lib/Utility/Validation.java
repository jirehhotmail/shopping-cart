package com.test.jireh.jireh_lib.Utility;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by Muthamizhan C on 7/13/2017.
 */

public class Validation {

    private static final String EMAIL_REG = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REG = "\\d{3}-\\d{7}";
    private static final String EMAIL_MSG = "Invalid email";
    private static final String REQUIRED_MSG = "Field required";
    private static final String PHONE_REQ = "Phone no invalid";

    public static boolean isEmailIdValid(EditText edtValue, boolean isRequired) {
        return isValid(edtValue, EMAIL_REG, EMAIL_MSG, isRequired);
    }

    public static boolean isValidPhone(EditText edtValue, boolean isRequired) {
        return isValidPhoneno(edtValue, PHONE_REG, PHONE_REQ, isRequired);
    }

    public static boolean isValidPhoneno(EditText edtValue, String phone_no, String emailMsg, boolean isRequired) {
        String edit_text = edtValue.getText().toString().trim();
        edtValue.setError(null);
        if (isRequired && !hasText(edtValue)) {
            return false;
        } else if (isRequired && (edit_text.length() ==10)) {
            edtValue.setError(phone_no);
            return false;
        }
        return true;
    }
    public static boolean isValid(EditText edtValue, String emailReg, String emailMsg, boolean isRequired) {
        String edit_text = edtValue.getText().toString().trim();
        edtValue.setError(null);
        if (isRequired && !hasText(edtValue)) {
            return false;
        } else if (isRequired && !Pattern.matches(emailReg, edit_text)) {
            edtValue.setError(emailMsg);
            return false;
        }
        return true;
    }

    public static boolean hasText(EditText edtValue) {
        String editText = edtValue.getText().toString();
        edtValue.setError(null);

        if (editText.length() == 0) {
            edtValue.setError(REQUIRED_MSG);
            return false;
        }
        return true;
    }

}
