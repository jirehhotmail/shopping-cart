package com.test.jireh.jireh_lib.Model;

/**
 * Created by Muthamizhan C on 25-07-2017.
 */

public class Cart {

    int product_id;
    int category_id;
    int product_qty;
    double product_price;
    String product_name;
    String product_dimage;


    public Cart(int product_id, int category_id, int product_qty, double product_price) {
        this.product_id = product_id;
        this.category_id = category_id;
        this.product_qty = product_qty;
        this.product_price = product_price;
    }

    public Cart(int product_qty, double product_price, int product_id, int category_id, String product_name, String product_dimage) {
        this.product_qty = product_qty;
        this.product_price = product_price;
        this.product_id = product_id;
        this.category_id = category_id;
        this.product_name = product_name;
        this.product_dimage =product_dimage;

    }

    public Cart() {

    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_dimage() {
        return product_dimage;
    }

    public void setProduct_dimage(String product_dimage) {
        this.product_dimage = product_dimage;
    }

    public double getProduct_price() {
        return product_price;
    }

    public void setProduct_price(double product_price) {
        this.product_price = product_price;
    }


    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(int product_qty) {
        this.product_qty = product_qty;
    }


}
