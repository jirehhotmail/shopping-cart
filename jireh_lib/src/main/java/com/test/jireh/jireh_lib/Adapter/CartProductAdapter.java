package com.test.jireh.jireh_lib.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.jireh.jireh_lib.Model.Cart;
import com.test.jireh.jireh_lib.R;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 25-07-2017.
 */

public class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.MyViewHolder> {
    ArrayList<Cart> cartDetails;
    Context context;

    public CartProductAdapter(ArrayList<Cart> cartDetails, Activity cartPage) {
        this.cartDetails = cartDetails;
        this.context = cartPage;

    }

    @Override
    public CartProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Cart cart = cartDetails.get(position);
        holder.product_name.setText(cart.getProduct_name());
        holder.product_price.setText(String.valueOf(cart.getProduct_price() * Double.valueOf(cart.getProduct_qty())));
        holder.product_qty.setText(String.valueOf(cart.getProduct_qty()));
    }

    @Override
    public int getItemCount() {
        return cartDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, product_price, product_qty, add_cart_txt, subtract_cart_txt;


        public MyViewHolder(View itemView) {
            super(itemView);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            product_price = (TextView) itemView.findViewById(R.id.product_price);
            product_qty = (TextView) itemView.findViewById(R.id.product_qty);
            add_cart_txt = (TextView) itemView.findViewById(R.id.add_cart_txt);
            subtract_cart_txt = (TextView) itemView.findViewById(R.id.subtract_cart_txt);
        }
    }
}
