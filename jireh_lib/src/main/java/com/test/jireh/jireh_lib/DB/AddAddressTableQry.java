package com.test.jireh.jireh_lib.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.jireh.jireh_lib.Model.Customer;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 28-07-2017.
 */

public class AddAddressTableQry {
    private static final String TABLE_ADDRESS = "ADDRESS";
    private static final String COLUMN_SNO = "S_NO";
    private static final String COLUMN_CUSTOMER_ID = "CUSTOMER_ID";
    private static final String COLUMN_FIRST_NAME = "FIRST_NAME";
    private static final String COLUMN_MOBILE_NO = "MOBILE_NO";
    private static final String COLUMN_ADDRESS1 = "ADDRESS1";
    private static final String COLUMN_ADDRESS2 = "ADDRESS2";
    private static final String COLUMN_CITY = "CITY";
    private static final String COLUMN_PINCODE = "PINCODE";
    private static final String COLUMN_STATE = "STATE";

    public void insertIntoAddress(Customer customer, Context context) {
        String sql = " INSERT INTO " + TABLE_ADDRESS + " VALUES (?,?,?,?,?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        SQLiteStatement statement = sqLiteDatabase.compileStatement(sql);
        sqLiteDatabase.beginTransaction();
        statement.clearBindings();
        statement.bindString(2, customer.getCustomer_id());
        statement.bindString(3, customer.getFirst_name());
        statement.bindString(4, customer.getMobile_no());
        statement.bindString(5, customer.getAddress1());
        statement.bindString(6, customer.getAddress2());
        statement.bindString(7, customer.getCity());
        statement.bindString(8, customer.getPincode());
        statement.bindString(9, customer.getState());
        statement.execute();
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public ArrayList<Customer> getAllAddressbyCustomerId(int customerId, Context context) {
        String sql = "SELECT * FROM " + TABLE_ADDRESS + " where " + COLUMN_CUSTOMER_ID + " = " + customerId;
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        ArrayList<Customer> addressList = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    Customer customer = new Customer();
                    customer.setFirst_name(cursor.getString(cursor.getColumnIndex(COLUMN_FIRST_NAME)));
                    customer.setMobile_no(cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE_NO)));
                    customer.setAddress1(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS1)));
                    customer.setAddress2(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS2)));
                    customer.setCity(cursor.getString(cursor.getColumnIndex(COLUMN_CITY)));
                    customer.setPincode(cursor.getString(cursor.getColumnIndex(COLUMN_PINCODE)));
                    customer.setState(cursor.getString(cursor.getColumnIndex(COLUMN_STATE)));
                    addressList.add(customer);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        sqLiteDatabase.close();
        return addressList;
    }

}
