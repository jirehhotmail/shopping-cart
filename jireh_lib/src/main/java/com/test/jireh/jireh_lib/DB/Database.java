package com.test.jireh.jireh_lib.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by UID-01 on 7/13/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 18;

    private static final String DATABASE_NAME = "ONLINE_DB.db";

    private static final String TABLE_CART = "CART";
    private static final String TABLE_PRODUCT = "PRODUCT";
    private static final String TABLE_CATEGORY = "CATEGORY";
    private static final String TABLE_SUB_CATEGORY = "SUB_CATEGORY";
    private static final String TABLE_CUSTOMER = "CUSTOMER";
    private static final String TABLE_ADDRESS = "ADDRESS";

    private static final String COLUMN_PRODUCT_ID = "PRODUCT_ID";
    private static final String COLUMN_PRODUCT_NAME = "PRODUCT_NAME";
    private static final String COLUMN_PRODUCT_IMAGE = "PRODUCT_IMG";
    private static final String COLUMN_PRODUCT_DIMAGE = "PRODUCT_DIMG";
    private static final String COLUMN_PRODUCT_PRICE = "PRODUCT_PRICE";
    private static final String COLUMN_PRODUCT_DPRICE = "PRODUCT_DISCOUNT_PRICE";
    private static final String COLUMN_PRODUCT_CATID = "CATEGORY_ID";
    private static final String COLUMN_STATUS = "PRODUCT_STATUS";
    private static final String COLUMN_PRODUCT_SIMAGE = "PRODUCT_SIMG";
    private static final String COLUMN_PRODUCT_HIMAGE = "PRODUCT_HIMG";
    private static final String COLUMN_PRODUCT_VIMAGE = "PRODUCT_VIMG";


    private static final String COLUMN_CATEGORYID = "CATEGORY_ID";
    private static final String COLUMN_CATEGORY_NAME = "CATEGORY_NAME";
    private static final String COLUMN_CATEGORY_IMAGE = "CATEGORY_IMAGE";

    private static final String COLUMN_SUB_CATEGORYID = "SUB_CATEGORYID";
    private static final String COLUMN_SUB_CATEGORY_NAME = "SUB_CATEGORY_NAME";
    private static final String COLUMN_SUB_CATEGORY_IMAGE = "SUB_CATEGORY_IMAGE";

    private static final String COLUMN_SNO = "S_NO";
    private static final String COLUMN_PDT_QTY = "PRODUCT_QTY";
    private static final String COLUMN_PDT_AMT = "PRODUCT_AMT";

    private static final String COLUMN_FIRST_NAME = "FIRST_NAME";
    private static final String COLUMN_LAST_NAME = "LAST_NAME";
    private static final String COLUMN_EMAIL_ID = "EMAIL_ID";
    private static final String COLUMN_MOBILE_NO = "MOBILE_NO";
    private static final String COLUMN_PASSWORD = "PASSWORD";
    private static final String COLUMN_ADDRESS1 = "ADDRESS1";
    private static final String COLUMN_ADDRESS2 = "ADDRESS2";
    private static final String COLUMN_CITY = "CITY";
    private static final String COLUMN_PINCODE = "PINCODE";
    private static final String COLUMN_STATE = "STATE";
    private static final String COLUMN_COUNTRY = "COUNTRY";

    private static final String COLUMN_CUSTOMER_ID ="CUSTOMER_ID";


    private static Database dbInstance = null;


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static Database getDbInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = new Database(context.getApplicationContext());
        }
        return dbInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table_category = "CREATE TABLE " + TABLE_CATEGORY + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_CATEGORYID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE, "
                + COLUMN_CATEGORY_NAME + " VARCHAR, "
                + COLUMN_CATEGORY_IMAGE + " VARCHAR " + ")";

        String create_table_sub_category = "CREATE TABLE " + TABLE_SUB_CATEGORY + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_CATEGORYID + " INTEGER , "
                + COLUMN_SUB_CATEGORYID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE, "
                + COLUMN_SUB_CATEGORY_NAME + " VARCHAR, "
                + COLUMN_SUB_CATEGORY_IMAGE + " VARCHAR " + ")";

        String create_table_product = "CREATE TABLE " + TABLE_PRODUCT + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_PRODUCT_ID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE, "
                + COLUMN_PRODUCT_NAME + " VARCHAR, "
                + COLUMN_PRODUCT_PRICE + " REAL, "
                + COLUMN_PRODUCT_DPRICE + " REAL, "
                + COLUMN_PRODUCT_CATID + " VARCHAR, "
                + COLUMN_STATUS + " VARCHAR,"
                + COLUMN_PRODUCT_IMAGE + " VARCHAR, "
                + COLUMN_PRODUCT_DIMAGE + " VARCHAR, "
                + COLUMN_PRODUCT_SIMAGE + " VARCHAR, "
                + COLUMN_PRODUCT_HIMAGE + " VARCHAR, "
                + COLUMN_PRODUCT_VIMAGE + " VARCHAR " + ")";

        String create_table_cart = "CREATE TABLE " + TABLE_CART + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_PDT_QTY + " INTEGER, "
                + COLUMN_PDT_AMT + " REAL, "
                + COLUMN_PRODUCT_ID + " INTEGER NOT NULL UNIQUE ON CONFLICT REPLACE, "
                + COLUMN_CATEGORYID + " INTEGER " + ")";

        String create_table_customer = "CREATE TABLE " + TABLE_CUSTOMER + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_FIRST_NAME + " VARCHAR, "
                + COLUMN_LAST_NAME + " VARCHAR, "
                + COLUMN_EMAIL_ID + " VARCHAR, "
                + COLUMN_MOBILE_NO + " VARCHAR, "
                + COLUMN_PASSWORD + " VARCHAR, "
                + COLUMN_ADDRESS1 + " VARCHAR, "
                + COLUMN_ADDRESS2 + " VARCHAR, "
                + COLUMN_CITY + " VARCHAR, "
                + COLUMN_PINCODE + " VARCHAR, "
                + COLUMN_STATE + " VARCHAR, "
                + COLUMN_COUNTRY + " VARCHAR " + ")";

        String create_table_address = "CREATE TABLE " + TABLE_ADDRESS + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_CUSTOMER_ID + " VARCHAR, "
                + COLUMN_FIRST_NAME + " VARCHAR, "
                + COLUMN_MOBILE_NO + " VARCHAR, "
                + COLUMN_ADDRESS1 + " VARCHAR, "
                + COLUMN_ADDRESS2 + " VARCHAR, "
                + COLUMN_CITY + " VARCHAR, "
                + COLUMN_PINCODE + " VARCHAR, "
                + COLUMN_STATE + " VARCHAR " + ")";

        db.execSQL(create_table_cart);
        db.execSQL(create_table_category);
        db.execSQL(create_table_product);
        db.execSQL(create_table_sub_category);
        db.execSQL(create_table_customer);
        db.execSQL(create_table_address);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        onCreate(db);
    }
}
