package com.test.jireh.jireh_lib.Adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.test.jireh.jireh_lib.Model.Product;
import com.test.jireh.jireh_lib.R;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    ArrayList<Product> products;
    ProductAdapter.OnItemClickListener listener;
    Activity activity;

    public ProductAdapter(ArrayList<Product> products, ProductAdapter.OnItemClickListener listener,Activity activity) {
        this.products = products;
        this.listener = listener;
        this.activity =activity;
    }

    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myHolderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row, parent, false);


        return new MyViewHolder(myHolderView);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.MyViewHolder holder, final int position) {
        Product product = products.get(position);
        holder.product_img.setImageResource(product.getProduct_image());
        holder.product_name_txt.setText(product.getProduct_name());
        holder.product_price.setText(activity.getResources().getString(R.string.rupee_symbol) + " " + String.valueOf(product.getProduct_price()));
        holder.product_price.setPaintFlags(holder.product_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.product_dprice.setText(activity.getResources().getString(R.string.rupee_symbol)+ " " + String.valueOf(product.getProduct_dprice()));
        holder.product_status.setText(product.getProduct_status());
        holder.add_cart_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.product_row_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClick(R.id.product_row_layout, position);
            }
        });
        holder.ratingBar.setRating(4.0f);

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public interface OnItemClickListener {
        void OnItemClick(int var1, int var2);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView product_img;
        TextView add_cart_txt, quantity_txt, subtract_cart_txt, product_name_txt, product_price, product_dprice, product_status;
        RatingBar ratingBar;
        RelativeLayout product_row_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            product_img = (ImageView) itemView.findViewById(R.id.product_img);
            add_cart_txt = (TextView) itemView.findViewById(R.id.add_cart_txt);
            quantity_txt = (TextView) itemView.findViewById(R.id.quantity_txt);
            subtract_cart_txt = (TextView) itemView.findViewById(R.id.subtract_cart_txt);
            product_name_txt = (TextView) itemView.findViewById(R.id.product_name_txt);
            product_price = (TextView) itemView.findViewById(R.id.product_price);
            product_dprice = (TextView) itemView.findViewById(R.id.product_dprice);
            product_row_layout = (RelativeLayout) itemView.findViewById(R.id.product_row_layout);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating_bar);
            product_status = (TextView) itemView.findViewById(R.id.product_status);
        }
    }
}
