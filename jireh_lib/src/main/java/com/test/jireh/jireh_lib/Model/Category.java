package com.test.jireh.jireh_lib.Model;

/**
 * Created by Muthamizhan C on 7/14/2017.
 */

public class Category {
    String category_name;
    int category_id;
    int category_image;
    int sub_category_id;

    public Category(String category_name, int category_id, int category_image) {
        this.category_name = category_name;
        this.category_id = category_id;
        this.category_image = category_image;
    }
    public Category(int category_id, String category_name, int sub_category_id, int category_image) {
        this.category_id = category_id;
        this.category_name = category_name;
        this.sub_category_id =sub_category_id;
        this.category_image = category_image;
    }

    public Category() {

    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getCategory_image() {
        return category_image;
    }

    public void setCategory_image(int category_image) {
        this.category_image = category_image;
    }


}
