package com.test.jireh.jireh_lib.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.jireh.jireh_lib.Model.Cart;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 25-07-2017.
 */

public class CartTableQry {

    private static final String TABLE_CART = "CART";
    private static final String TABLE_PRODUCT = "PRODUCT";

    private static final String COLUMN_SNO = "S_NO";
    private static final String COLUMN_PDT_QTY = "PRODUCT_QTY";
    private static final String COLUMN_PDT_AMT = "PRODUCT_AMT";

    private static final String COLUMN_PRODUCT_ID = "PRODUCT_ID";
    private static final String COLUMN_CATEGORYID = "CATEGORY_ID";

    public void insertIntoCartTable(Cart pdtList, Context context) {
        String sql = "INSERT INTO " + TABLE_CART + " VALUES (?,?,?,?,?);";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        SQLiteStatement statement = sqLiteDatabase.compileStatement(sql);
        sqLiteDatabase.beginTransaction();
        statement.clearBindings();
        statement.bindLong(2, pdtList.getProduct_qty());
        statement.bindDouble(3, pdtList.getProduct_price());
        statement.bindLong(4, pdtList.getProduct_id());
        statement.bindLong(5, pdtList.getCategory_id());
        statement.execute();
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

    }

    public int insertorUpdateCartTable(Cart pdtDetail, Context context) {
        Cart cart = getProductDetail(pdtDetail, context);
        if (cart != null) {
            int qty = cart.getProduct_qty();
            qty = qty + 1;
            cart.setProduct_qty(qty);
            updateCartTable(cart, context);
            return cart.getProduct_qty();
        } else {
            insertIntoCartTable(pdtDetail, context);
            return pdtDetail.getProduct_qty();

        }

    }

    private int updateCartTable(Cart cart, Context context) {
        Database db = Database.getDbInstance(context);
        SQLiteDatabase sqldb = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PDT_QTY, cart.getProduct_qty());
        contentValues.put(COLUMN_PRODUCT_ID, cart.getProduct_id());

        return sqldb.update(TABLE_CART, contentValues, COLUMN_PRODUCT_ID + " = ? ", new String[]{String.valueOf(cart.getProduct_id())});
    }

    private Cart getProductDetail(Cart pdtDetail, Context context) {
        String sql = "SELECT * FROM " + TABLE_CART + " WHERE " + COLUMN_PRODUCT_ID + " = " + pdtDetail.getProduct_id();
        Database db = Database.getDbInstance(context);
        SQLiteDatabase database = db.getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Cart cart = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                cart = new Cart(cursor.getInt(3), cursor.getInt(4), cursor.getInt(1), cursor.getDouble(2));
            }
            cursor.close();
        }

        db.close();
        return cart;
    }

    public ArrayList<Cart> getAllCartProductDetail(Context context) {
        String sql = "SELECT ca.product_qty,ca.product_amt,ca.product_id,ca.category_id,pr.product_name,pr.product_dimg from cart ca,product pr WHERE " + "ca." + COLUMN_PRODUCT_ID + " = pr." + COLUMN_PRODUCT_ID;
        Database db = Database.getDbInstance(context);
        SQLiteDatabase database = db.getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        ArrayList<Cart> cartDetails = new ArrayList<>();
        Cart cart;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    cart = new Cart(cursor.getInt(0), cursor.getDouble(1), cursor.getInt(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5));
                    cartDetails.add(cart);

                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        db.close();
        return cartDetails;
    }

    public int getCartQuantity(Context context) {
        String sql = " SELECT SUM(PRODUCT_QTY) FROM " + TABLE_CART + ";";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        int qty = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                qty = cursor.getInt(0);
            }
            cursor.close();
        }
        database.close();
        return qty;
    }

    public double getCartTotal(Context context) {
        String sql = "SELECT SUM(PRODUCT_QTY * PRODUCT_AMT) AS TOTAL_AMT FROM  " + TABLE_CART + ";";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        double totalAmt = 0.0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                totalAmt = cursor.getDouble(0);
            }
            cursor.close();
        }
        database.close();
        return totalAmt;
    }
}
