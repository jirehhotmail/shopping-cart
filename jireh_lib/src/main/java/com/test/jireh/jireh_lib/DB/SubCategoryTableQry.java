package com.test.jireh.jireh_lib.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.jireh.jireh_lib.Model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 19-07-2017.
 */

public class SubCategoryTableQry {

    private static final String TABLE_SUB_CATEGORY = "SUB_CATEGORY";

    private static final String COLUMN_CATEGORYID = "CATEGORY_ID";
    private static final String COLUMN_SUB_CATEGORYID = "SUB_CATEGORYID";
    private static final String COLUMN_SUB_CATEGORY_NAME = "SUB_CATEGORY_NAME";
    private static final String COLUMN_SUB_CATEGORY_IMAGE = "SUB_CATEGORY_IMAGE";

    public void insertIntoSubCategory(List<Category> categoryList, Context context) {
        String sql = "INSERT INTO " + TABLE_SUB_CATEGORY + " VALUES (?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();

        for (int idx = 0; idx < categoryList.size(); idx++) {
            statement.clearBindings();
            statement.bindLong(2, categoryList.get(idx).getCategory_id());
            statement.bindLong(3, categoryList.get(idx).getSub_category_id());
            statement.bindString(4, categoryList.get(idx).getCategory_name());
            statement.bindLong(5, categoryList.get(idx).getCategory_image());
            statement.execute();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public ArrayList<Category> getAllSubCategory(Context context) {
        String sql = "SELECT * FROM " + TABLE_SUB_CATEGORY + ";";
        Database db = Database.getDbInstance(context);
        SQLiteDatabase database = db.getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        ArrayList<Category> categories = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setCategory_id(cursor.getInt(cursor.getColumnIndex(COLUMN_CATEGORYID)));
                category.setSub_category_id(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CATEGORYID)));
                category.setCategory_name(cursor.getString(cursor.getColumnIndex(COLUMN_SUB_CATEGORY_NAME)));
                category.setCategory_image(cursor.getInt(cursor.getColumnIndex(COLUMN_SUB_CATEGORY_IMAGE)));
                categories.add(category);
            } while (cursor.moveToNext());
        }
        return categories;
    }
}
