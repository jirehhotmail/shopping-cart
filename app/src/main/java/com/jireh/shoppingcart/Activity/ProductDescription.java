package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.shoppingcart.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.test.jireh.jireh_lib.App.Constants;
import com.test.jireh.jireh_lib.DB.CartTableQry;
import com.test.jireh.jireh_lib.Interface.CartTrigger;
import com.test.jireh.jireh_lib.Model.Cart;
import com.test.jireh.jireh_lib.Model.Product;


/**
 * Created by Muthamizhan C on 21-07-2017.
 */

public class ProductDescription extends Activity implements CartTrigger.cartChangeListener {
    Toolbar toolbar;
    ImageView product_img, back_img, product_img_s, product_img_h, product_img_v;
    TextView title_text, pdt_name_text, pdt_price_text, pdt_dprice_text, cart_total_qty;
    RatingBar ratingBar;
    Button add_cart_btn;
    Product product;
    RelativeLayout cart_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_description);
        initializeVariable();
        setOnClickListener();

        CartTrigger.getInstance().setCustomObjectReference(this);

        product = (Product) getIntent().getSerializableExtra("productDetail");

        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier(product.getProduct_dimage(), "drawable", this.getPackageName());
        product_img.setImageDrawable(resources.getDrawable(resourceId));

        int resourceId1 = resources.getIdentifier(product.getProduct_simage(), "drawable", this.getPackageName());
        product_img_s.setImageDrawable(resources.getDrawable(resourceId1));


        int resourceId2 = resources.getIdentifier(product.getProduct_himage(), "drawable", this.getPackageName());
        product_img_h.setImageDrawable(resources.getDrawable(resourceId2));


        int resourceId3 = resources.getIdentifier(product.getProduct_vimage(), "drawable", this.getPackageName());
        product_img_v.setImageDrawable(resources.getDrawable(resourceId3));

        pdt_name_text.setText(product.getProduct_name());

        pdt_price_text.setText(getResources().getString(R.string.rupee_symbol) + " " + String.valueOf(product.getProduct_price()));
        pdt_price_text.setPaintFlags(pdt_price_text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        pdt_dprice_text.setText(getResources().getString(R.string.rupee_symbol) + " " + String.valueOf(product.getProduct_dprice()));
        ratingBar.setRating(4.0f);

    }

    protected void onResume() {
        super.onResume();
        try {
            CartTableQry qry = new CartTableQry();
            int qty = qry.getCartQuantity(ProductDescription.this);
            cart_total_qty.setText(String.valueOf(qty));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        add_cart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cart cart = new Cart();
                cart.setProduct_id(product.getProduct_id());
                cart.setProduct_qty(1);
                cart.setProduct_price(product.getProduct_dprice());
                cart.setCategory_id(product.getProduct_catid());
                CartTableQry tableQry = new CartTableQry();
                tableQry.insertorUpdateCartTable(cart, ProductDescription.this);
                Toast.makeText(ProductDescription.this, "Product added to cart", Toast.LENGTH_LONG).show();
                CartTrigger.getInstance().triggerCartChange();

            }
        });
        product_img_s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources resources = ProductDescription.this.getResources();
                int resourceId1 = resources.getIdentifier(product.getProduct_simage(), "drawable", ProductDescription.this.getPackageName());
                product_img.setImageDrawable(resources.getDrawable(resourceId1));
            }
        });
        product_img_h.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources resources = ProductDescription.this.getResources();
                int resourceId1 = resources.getIdentifier(product.getProduct_himage(), "drawable", ProductDescription.this.getPackageName());
                product_img.setImageDrawable(resources.getDrawable(resourceId1));
            }
        });
        product_img_v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources resources = ProductDescription.this.getResources();
                int resourceId1 = resources.getIdentifier(product.getProduct_vimage(), "drawable", ProductDescription.this.getPackageName());
                product_img.setImageDrawable(resources.getDrawable(resourceId1));
            }
        });
        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Prefs.getInt(Constants.customer_id, 0) != 0) {
                    Intent in = new Intent(ProductDescription.this, CartPage.class);
                    startActivity(in);
                } else {
                    Intent in = new Intent(ProductDescription.this, Login.class);
                    startActivity(in);
                }
            }
        });

    }

    private void initializeVariable() {
        product_img = (ImageView) findViewById(R.id.product_img);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
        pdt_name_text = (TextView) findViewById(R.id.pdt_name_text);
        pdt_price_text = (TextView) findViewById(R.id.pdt_price_text);
        ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        add_cart_btn = (Button) findViewById(R.id.add_cart_btn);
        pdt_dprice_text = (TextView) findViewById(R.id.pdt_dprice_text);
        product_img_s = (ImageView) findViewById(R.id.product_img_s);
        product_img_h = (ImageView) findViewById(R.id.product_img_h);
        product_img_v = (ImageView) findViewById(R.id.product_img_v);
        cart_total_qty = (TextView) findViewById(R.id.cart_total_qty);
        cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
    }

    @Override
    public void callback() {
        updateCartQty();
    }

    private void updateCartQty() {
        CartTableQry cartTableQry = new CartTableQry();
        int qty = cartTableQry.getCartQuantity(ProductDescription.this);
        cart_total_qty.setText(String.valueOf(qty));
    }
}
