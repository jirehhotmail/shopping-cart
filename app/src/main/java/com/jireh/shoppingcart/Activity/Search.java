package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.shoppingcart.R;
import com.test.jireh.jireh_lib.Adapter.ProductAdapter;
import com.test.jireh.jireh_lib.Model.Product;

import java.io.Serializable;
import java.util.ArrayList;

public class Search extends Activity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text;
    ProductAdapter adapter;
    RecyclerView search_recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initialiseVariable();
        setOnClickListener();

        final ArrayList<Product> productList = (ArrayList<Product>) getIntent().getSerializableExtra("SearchList");
        adapter = new ProductAdapter(productList, new ProductAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int var1, int position) {
                if (var1 == R.id.product_row_layout) {
                    Product product = productList.get(position);
                    Intent in = new Intent(Search.this, ProductDescription.class);
                    in.putExtra("productDetail", (Serializable) product);
                    startActivity(in);
                }


            }
        }, Search.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Search.this);
        search_recycler.setLayoutManager(layoutManager);
        search_recycler.setAdapter(adapter);


    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initialiseVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
        search_recycler = (RecyclerView) findViewById(R.id.search_recycler);
    }
}
