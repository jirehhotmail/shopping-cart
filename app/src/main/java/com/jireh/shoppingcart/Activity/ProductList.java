package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.shoppingcart.R;
import com.test.jireh.jireh_lib.Adapter.ProductAdapter;
import com.test.jireh.jireh_lib.DB.ProductTableQry;
import com.test.jireh.jireh_lib.Model.Product;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class ProductList extends Activity {
    RecyclerView product_list_recycler;
    ArrayList<Product> productList = new ArrayList<>();
    ProductAdapter adapter;
    int subCategoryId;
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_layout);
        intializeVariable();
        setOnClickListener();


        subCategoryId = getIntent().getIntExtra("subCategoryId", 0);
        title_text.setText(getIntent().getStringExtra("subCategoryName"));

        prepareProductList(subCategoryId);
    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void intializeVariable() {
        product_list_recycler = (RecyclerView) findViewById(R.id.product_list_recycler);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);

    }


    private void prepareProductList(int subCategoryId) {
        productList.clear();
        if (subCategoryId == 1) {
            Product product = new Product(1, "Carrot ooty ", 1, R.drawable.carrot_type1,120.00, 80.00, "In Stock", "carrot_type1", "carrot_type_s", "carrot_type_h", "carrot_type_v");

            productList.add(product);

            product = new Product(2, "Carrot Normal ", 1, R.drawable.carrot_type2,  130.00, 100.00, "In Stock", "carrot_type2", "carrot_type2_s", "carrot_type2_h", "carrot_type2_v");

            productList.add(product);

            product = new Product(3, "Carrot Purple ", 1, R.drawable.carrot_type3, 40.00, 20.00, "In Stock", "carrot_type3", "carrot_type3_s", "carrot_type3_h", "carrot_type3_v");

            productList.add(product);
        } else if (subCategoryId == 2) {
            Product product = new Product(4, "Onion small ", 2, R.drawable.onion_type1, 70.00, 50.00, "In Stock", "onion_type1", "onion_type1_s", "onion_type1_h", "onion_type1_v");

            productList.add(product);

            product = new Product(5, "Onion white ", 2, R.drawable.onion_type2, 50.00, 40.00, "In Stock", "onion_type2", "onion_type2_s", "onion_type2_h", "onion_type2_v");

            productList.add(product);


        } else if (subCategoryId == 3) {

            Product product = new Product(6, "Sugar Apple Red", 3, R.drawable.seetha_type1, 70.00, 60.00, "In Stock", "seetha_type1", "seetha_type1_s", "seetha_type1_h", "seetha_type1_v");

            productList.add(product);

            product = new Product(7, "Sugar Apple", 3, R.drawable.seetha_type2,80.00, 65.00, "In Stock", "seetha_type2", "seetha_type2_s", "seetha_type2_h", "seetha_type2_v");

            productList.add(product);

        } else if (subCategoryId == 4) {


            Product product = new Product(8, "Guava Green ", 4, R.drawable.guava_type1, 50.00, 40.00, "In Stock", "guava_type1", "guava_type1_s", "guava_type1_h", "guava_type1_v");

            productList.add(product);

            product = new Product(9, "Guava Red", 4, R.drawable.guava_type2, 90.00, 70.00, "In Stock", "guava_type2", "guava_type2_s", "guava_type2_h", "guava_type2_v");

            productList.add(product);


        } else if (subCategoryId == 5) {

            Product product = new Product(10, "Travel bag", 5, R.drawable.travel_type1, 195.00, 170.00, "In Stock", "travel_type1", "travel_type1_s", "travel_type1_h", "travel_type1_v");

            productList.add(product);
            product = new Product(11, "Travel bag big", 5, R.drawable.travel_type2,200.00, 170.00, "In Stock", "travel_type2", "travel_type2_s", "travel_type2_h", "travel_type2_v");

            productList.add(product);
        } else if (subCategoryId == 7) {
            Product product = new Product(12, "Baniyan black", 7, R.drawable.shirt_type1,200.00, 170.00, "In Stock", "shirt_type1", "shirt_type1_s", "shirt_type1_h", "shirt_type1_v");

            productList.add(product);
            product = new Product(13, "Baniyan grey", 7, R.drawable.shirt_type2, 200.00, 175.00, "In Stock", "shirt_type2", "shirt_type2_s", "shirt_type2_h", "shirt_type2_v");

            productList.add(product);

            product = new Product(14, "Baniyan white", 7, R.drawable.shirt_type3, 205.00, 175.00, "In Stock", "shirt_type3", "shirt_type3_s", "shirt_type3_h", "shirt_type3_v");

            productList.add(product);
        } else if (subCategoryId == 8) {
            Product product = new Product(15, "Checked Shirt1 ", 8, R.drawable.shirt_checked2, 700.00, 550.00, "In Stock", "shirt_checked2", "shirt_checked2_s", "shirt_checked2_h", "shirt_checked2_v");

            productList.add(product);

            product = new Product(16, "Checked Shirt2  ", 8, R.drawable.shirt_checked3, 500.00, 450.00, "In Stock", "shirt_checked3", "shirt_checked3_s", "shirt_checked3_h", "shirt_checked3_v");

            productList.add(product);


        } else if (subCategoryId == 9) {
            Product product = new Product(17, "Pant type 1 ", 9, R.drawable.pant_type1, 500.00, 470.00, "In Stock", "pant_type1", "pant_type1_s", "pant_type1_h", "pant_type1_v");

            productList.add(product);

            product = new Product(18, "Pant type 2", 9, R.drawable.pant_type2,  600.00, 560.00, "In Stock", "pant_type2", "pant_type2_s", "pant_type2_h", "pant_type2_v");

            productList.add(product);


        } else if (subCategoryId == 10) {
            Product product = new Product(19, "Jean pant 1", 10, R.drawable.pant_jean1, 1100.00, 1070.00, "In Stock", "pant_jean1", "pant_jean1_s", "pant_jean1_h", "pant_jean1_v");

            productList.add(product);

            product = new Product(20, "Jean pant 2", 10, R.drawable.pant_jean2,1000.00, 970.00, "In Stock", "pant_jean2", "pant_jean2_s", "pant_jean2_h", "pant_jean2_v");

            productList.add(product);


        } else if (subCategoryId == 11) {

            Product product = new Product(21, "Bureau wood1", 11, R.drawable.bureau_type1, 11100.00, 10000.00, "In Stock", "bureau_type1", "bureau_type1_s", "bureau_type1_h", "bureau_type1_v");

            productList.add(product);
            product = new Product(22, "Bureau wood2", 11, R.drawable.bureau_type2,10100.00, 9000.00, "In Stock", "bureau_type2", "bureau_type2_s", "bureau_type2_h", "bureau_type2_v");

            productList.add(product);
        } else if (subCategoryId == 13) {
            Product product = new Product(23, "Clock small ", 13, R.drawable.clock_type1, 300.00, 270.00, "In Stock", "clock_type1", "clock_type1_s", "clock_type1_h", "clock_type1_v");

            productList.add(product);
            product = new Product(24, "Clock", 13, R.drawable.clock_type2, 300.00, 250.00, "In Stock", "clock_type2", "clock_type2_s", "clock_type2_h", "clock_type2_v");

            productList.add(product);


        } else if (subCategoryId == 15) {
            Product product = new Product(25, "Lamp set 1", 15, R.drawable.lamp_type1,  300.00, 270.00, "In Stock", "lamp_type1", "lamp_type1_s", "lamp_type1_h", "lamp_type1_v");

            productList.add(product);
            product = new Product(26, "Lamp set 2", 15, R.drawable.lamp_type2,350.00, 250.00, "In Stock", "lamp_type2", "lamp_type2_s", "lamp_type2_h", "lamp_type2_v");

            productList.add(product);


        }
        adapter = new ProductAdapter(productList, new ProductAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int var1, int position) {
                if (var1 == R.id.product_row_layout) {
                    Product product = productList.get(position);
                    Intent in = new Intent(ProductList.this, ProductDescription.class);
                    in.putExtra("productDetail", (Serializable) product);
                    startActivity(in);
                }


            }
        }, ProductList.this);

        LinearLayoutManager lLM = new LinearLayoutManager(ProductList.this);
        product_list_recycler.setLayoutManager(lLM);

        product_list_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        ProductTableQry tableQry = new ProductTableQry();
        tableQry.insertProductDB(productList, ProductList.this);

    }
}
