package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.shoppingcart.R;
import com.jireh.shoppingcart.Util.Validation;
import com.pixplicity.easyprefs.library.Prefs;
import com.test.jireh.jireh_lib.App.Constants;
import com.test.jireh.jireh_lib.DB.CustomerTableQry;
import com.test.jireh.jireh_lib.Model.Customer;

/**
 * Created by Muthamizhan C on 7/13/2017.
 */

public class Registration extends Activity {

    EditText first_name_edt, last_name_edt, email_edt, phone_edt, password_edt, address1_edt, address2_edt,
             city_edt, pincode_edt, state_edt, country_edt;
    Button register_btn;
    boolean validation = true;
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        initVariable();
        setOnClickListener();
        title_text.setText("Registration");
        //To hide pop up keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setOnClickListener() {
        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation_fields()) {
                    Customer customer = new Customer();
                    customer.setFirst_name(first_name_edt.getText().toString().trim());
                    customer.setLast_name(last_name_edt.getText().toString().trim());
                    customer.setEmail_id(email_edt.getText().toString().trim());
                    customer.setMobile_no(phone_edt.getText().toString().trim());
                    customer.setPassword(password_edt.getText().toString().trim());
                    customer.setAddress1(address1_edt.getText().toString());
                    customer.setAddress2(address2_edt.getText().toString());
                    customer.setCity(city_edt.getText().toString());
                    customer.setPincode(pincode_edt.getText().toString());
                    customer.setState(state_edt.getText().toString());
                    customer.setCountry(country_edt.getText().toString());
                    CustomerTableQry customerTableQry = new CustomerTableQry();
                    customerTableQry.insertCustomer(customer, Registration.this);
                    Toast.makeText(Registration.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                    int customerId = customerTableQry.userCheck(email_edt.getText().toString().trim(), password_edt.getText().toString().trim(), Registration.this);
                    Prefs.putInt(Constants.customer_id, customerId);
                    finish();
                }
            }
        });
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private boolean validation_fields() {
        validation = true;
        if (!Validation.hasText(first_name_edt))
            validation = false;
        if (!Validation.hasText(last_name_edt))
            validation = false;
        if (!Validation.isEmailIdValid(email_edt, true))
            validation = false;
        if (!Validation.isValidPhone(phone_edt, true))
            validation = false;
        if (!Validation.isValidPassword(password_edt, true))
            validation = false;
        if (!Validation.hasText(address1_edt))
            validation = false;
        if (!Validation.hasText(address2_edt))
            validation = false;
        if (!Validation.hasText(city_edt))
            validation = false;
        if(!Validation.hasText(pincode_edt))
            validation =false;
        if (!Validation.hasText(state_edt))
            validation = false;
        if (!Validation.hasText(country_edt))
            validation = false;

        return validation;
    }

    private void initVariable() {
        first_name_edt = (EditText) findViewById(R.id.first_name_edt);
        last_name_edt = (EditText) findViewById(R.id.last_name_edt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        phone_edt = (EditText) findViewById(R.id.phone_edt);
        password_edt = (EditText) findViewById(R.id.password_edt);
        address1_edt = (EditText) findViewById(R.id.address1_edt);
        address2_edt = (EditText) findViewById(R.id.address2_edt);
        city_edt = (EditText) findViewById(R.id.city_edt);
        pincode_edt = (EditText) findViewById(R.id.pincode_edt);
        state_edt = (EditText) findViewById(R.id.state_edt);
        country_edt = (EditText) findViewById(R.id.country_edt);
        register_btn = (Button) findViewById(R.id.register_btn);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
    }
}
