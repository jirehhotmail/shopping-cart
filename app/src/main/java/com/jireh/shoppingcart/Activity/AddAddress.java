package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jireh.shoppingcart.R;
import com.jireh.shoppingcart.Util.Validation;

/**
 * Created by Muthamizhan C on 28-07-2017.
 */

public class AddAddress extends Activity {
    EditText first_name_edt, phone_edt, password_edt, address1_edt, address2_edt,
            city_edt, pincode_edt, state_edt;
    Button add_address_btn;
    boolean validation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address);
        initVariable();
        setOnClickListener();

    }

    private void setOnClickListener() {
        add_address_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation_fields()) {

                }
            }
        });
    }

    private boolean validation_fields() {
        validation = true;
        if (!Validation.hasText(first_name_edt))
            validation = false;
        if (!Validation.isValidPhone(phone_edt, true))
            validation = false;
        if (!Validation.isValidPassword(password_edt, true))
            validation = false;
        if (!Validation.hasText(address1_edt))
            validation = false;
        if (!Validation.hasText(address2_edt))
            validation = false;
        if (!Validation.hasText(city_edt))
            validation = false;
        if (!Validation.hasText(pincode_edt))
            validation = false;
        if (!Validation.hasText(state_edt))
            validation = false;

        return validation;
    }

    private void initVariable() {
        first_name_edt = (EditText) findViewById(R.id.first_name_edt);
        phone_edt = (EditText) findViewById(R.id.phone_edt);
        password_edt = (EditText) findViewById(R.id.password_edt);
        address1_edt = (EditText) findViewById(R.id.address1_edt);
        address2_edt = (EditText) findViewById(R.id.address2_edt);
        city_edt = (EditText) findViewById(R.id.city_edt);
        pincode_edt = (EditText) findViewById(R.id.pincode_edt);
        state_edt = (EditText) findViewById(R.id.state_edt);
        add_address_btn = (Button) findViewById(R.id.register_btn);

    }
}
