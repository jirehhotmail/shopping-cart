package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.shoppingcart.R;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class MyOrders extends Activity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myorders);
        initializeVariables();
        onClickListener();
        title_text.setText("My Orders");

    }

    private void onClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initializeVariables() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
    }
}
