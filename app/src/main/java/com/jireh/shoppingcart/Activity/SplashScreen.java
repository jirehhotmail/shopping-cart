package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.jireh.shoppingcart.R;


/**
 * Created by Muthamizhan C on 7/13/2017.
 */

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Intent in = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(in);
                finish();
            }
        }.start();
    }
}
