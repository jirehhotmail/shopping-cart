package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.shoppingcart.R;
import com.jireh.shoppingcart.Util.Validation;
import com.pixplicity.easyprefs.library.Prefs;
import com.test.jireh.jireh_lib.App.Constants;
import com.test.jireh.jireh_lib.DB.CustomerTableQry;

/**
 * Created by Muthamizhan C on 27-07-2017.
 */

public class Login extends Activity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text, register_txt;
    EditText email_edt, password_edt;
    Button login_btn;
    boolean validation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        initializeVariables();
        onClickListener();
        title_text.setText("Login");
    }

    private void onClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationLogin()) {
                    String emaiId = email_edt.getText().toString().trim();
                    String password = password_edt.getText().toString().trim();
                    CustomerTableQry tableQry = new CustomerTableQry();
                    int customerId = tableQry.userCheck(emaiId, password, Login.this);
                    if (customerId != 0) {
                        Intent in = new Intent(Login.this, MainActivity.class);
                        startActivity(in);
                        Toast.makeText(Login.this, "Logged in Successfully", Toast.LENGTH_SHORT).show();
                        Prefs.putInt(Constants.customer_id, customerId);
                        finish();
                    } else {
                        Toast.makeText(Login.this, "Invalid username/password", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        register_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Login.this, Registration.class);
                startActivity(in);
                Toast.makeText(Login.this, "Please register", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private boolean validationLogin() {
        validation = true;
        if (!Validation.hasText(email_edt))
            validation = false;
        if (!Validation.hasText(password_edt))
            validation = false;
        return validation;
    }

    private void initializeVariables() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
        register_txt = (TextView) findViewById(R.id.register_txt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        password_edt = (EditText) findViewById(R.id.password_edt);
        login_btn = (Button) findViewById(R.id.login_btn);
    }
}
