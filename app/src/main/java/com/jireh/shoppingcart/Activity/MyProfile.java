package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.shoppingcart.R;
import com.jireh.shoppingcart.Util.Validation;
import com.pixplicity.easyprefs.library.Prefs;
import com.test.jireh.jireh_lib.App.Constants;
import com.test.jireh.jireh_lib.DB.CustomerTableQry;
import com.test.jireh.jireh_lib.Model.Customer;

/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class MyProfile extends Activity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text;
    EditText person_name_edt, mobile_no_edt, email_edt;
    Button update_btn;
    boolean validation = true;
    int customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile);
        initializeVariables();
        onClickListener();
        title_text.setText("My Profile");
        //To hide pop up keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //get customer data
        CustomerTableQry tableQry = new CustomerTableQry();
        customerId = Prefs.getInt(Constants.customer_id, 0);
        Customer customer = tableQry.getCustomerData(customerId, MyProfile.this);
        if (customerId != 0) {
            person_name_edt.setText(customer.getFirst_name());
            mobile_no_edt.setText(customer.getMobile_no());
            email_edt.setText(customer.getEmail_id());
        }

    }

    private void onClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationProfile()) {
                    Customer customer = new Customer();
                    customer.setFirst_name(person_name_edt.getText().toString().trim());
                    customer.setEmail_id(email_edt.getText().toString().trim());
                    customer.setMobile_no(mobile_no_edt.getText().toString().trim());
                    CustomerTableQry tableQry = new CustomerTableQry();
                    int result = tableQry.updateCustomer(customer, customerId, MyProfile.this);
                    if (result == 1) {
                        Toast.makeText(MyProfile.this, "Updated Successfully" + result, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(MyProfile.this, "Something went wrong" +
                                "" + result, Toast.LENGTH_LONG).show();
                        finish();
                    }

                }
            }
        });
    }

    private boolean validationProfile() {
        validation = true;
        if (!Validation.isEmailIdValid(email_edt, true))
            validation = false;
        else if (!Validation.hasText(person_name_edt))
            validation = false;
        else if (!Validation.isValidPhone(mobile_no_edt, true))
            validation = false;
        return validation;
    }

    private void initializeVariables() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
        person_name_edt = (EditText) findViewById(R.id.person_name_edt);
        mobile_no_edt = (EditText) findViewById(R.id.mobile_no_edt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        update_btn = (Button) findViewById(R.id.update_btn);
    }
}
