package com.jireh.shoppingcart.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.shoppingcart.R;
import com.test.jireh.jireh_lib.Adapter.CartProductAdapter;
import com.test.jireh.jireh_lib.DB.CartTableQry;
import com.test.jireh.jireh_lib.Model.Cart;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 20-07-2017.
 */

public class CartPage extends Activity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_text, tax_amt_txt, totalamt_txt, gross_totalamt_txt;
    RecyclerView cart_recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_page);
        initialiseVariable();
        setOnClickListener();

        title_text.setText("Cart");
        CartTableQry tableQry = new CartTableQry();
        ArrayList<Cart> cartDetails = tableQry.getAllCartProductDetail(CartPage.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(CartPage.this);
        CartProductAdapter adapter = new CartProductAdapter(cartDetails, CartPage.this);
        cart_recycler.setLayoutManager(layoutManager);
        cart_recycler.setAdapter(adapter);
        //set Total
        CartTableQry cartQry = new CartTableQry();
        double totalAmt = cartQry.getCartTotal(CartPage.this);
        gross_totalamt_txt.setText(String.valueOf(totalAmt));
        double taxAmt = totalAmt * (12.0 / 100);
        tax_amt_txt.setText(String.valueOf(taxAmt));
        totalamt_txt.setText(String.valueOf(totalAmt + taxAmt));
    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void initialiseVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_text = (TextView) toolbar.findViewById(R.id.title_text);
        cart_recycler = (RecyclerView) findViewById(R.id.cart_recycler);
        totalamt_txt = (TextView) findViewById(R.id.totalamt_txt);
        tax_amt_txt = (TextView) findViewById(R.id.tax_amt_txt);
        gross_totalamt_txt = (TextView) findViewById(R.id.gross_totalamt_txt);
    }
}
