package com.jireh.shoppingcart.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.shoppingcart.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.test.jireh.jireh_lib.Adapter.CategoryAdapter;
import com.test.jireh.jireh_lib.Adapter.ProductAdapter;
import com.test.jireh.jireh_lib.App.Constants;
import com.test.jireh.jireh_lib.DB.CartTableQry;
import com.test.jireh.jireh_lib.DB.CategoryTableQry;
import com.test.jireh.jireh_lib.DB.ProductTableQry;
import com.test.jireh.jireh_lib.Model.Category;
import com.test.jireh.jireh_lib.Model.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Muthamizhan C on 7/13/2017.
 */
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PHONE_CALL = 1;
    RecyclerView recyclerView, search_recycler;
    List<Category> categoryList = new ArrayList<>();
    CategoryAdapter adapter;
    ProductAdapter pdtAdapter;
    Toolbar toolbar;
    ImageView search_img, cart_img;
    EditText search_edt;
    ArrayList<Product> productList = new ArrayList<>();
    RelativeLayout search_layout, main_layout;
    TextView cart_total_qty;
    private SliderLayout slider_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //To hide pop up keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        intializeVariable();
        setNavigationDrawer();
        setBanner();
        setOnClickListener();

        //set the adapter
        adapter = new CategoryAdapter(categoryList, new CategoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                //  Toast.makeText(MainActivity.this,"Position Clicked" + position,Toast.LENGTH_LONG).show();

                Intent in = new Intent(MainActivity.this, SubCategoryList.class);
                in.putExtra("categoryId", categoryList.get(position).getCategory_id());
                in.putExtra("categoryTitle", categoryList.get(position).getCategory_name());
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        GridLayoutManager manager = new GridLayoutManager(MainActivity.this, 2);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        prepareCategoryList();
        prepareProductList();


        //check permission
        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            CartTableQry qry = new CartTableQry();
            int qty = qry.getCartQuantity(MainActivity.this);
            cart_total_qty.setText(String.valueOf(qty));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareProductList() {


        productList.clear();

        Product product = new Product(1, "Carrot ooty ", 1, R.drawable.carrot_type1, 120.00, 80.00, "In Stock", "carrot_type1",
                "carrot_type_s", "carrot_type_h", "carrot_type_v");

        productList.add(product);

        product = new Product(2, "Carrot Normal ", 1, R.drawable.carrot_type2, 130.00, 100.00, "In Stock", "carrot_type2", "carrot_type2_s", "carrot_type2_h", "carrot_type2_v");

        productList.add(product);

        product = new Product(3, "Carrot Purple ", 1, R.drawable.carrot_type3, 40.00, 20.00, "In Stock", "carrot_type3", "carrot_type3_s", "carrot_type3_h", "carrot_type3_v");

        productList.add(product);

        product = new Product(4, "Onion small ", 2, R.drawable.onion_type1, 70.00, 50.00, "In Stock", "onion_type1", "onion_type1_s", "onion_type1_h", "onion_type1_v");

        productList.add(product);

        product = new Product(5, "Onion white ", 2, R.drawable.onion_type2, 50.00, 40.00, "In Stock", "onion_type2", "onion_type2_s", "onion_type2_h", "onion_type2_v");

        productList.add(product);

        product = new Product(6, "Sugar Apple Red", 3, R.drawable.seetha_type1, 70.00, 60.00, "In Stock", "seetha_type1", "seetha_type1_s", "seetha_type1_h", "seetha_type1_v");

        productList.add(product);

        product = new Product(7, "Sugar Apple", 3, R.drawable.seetha_type2, 80.00, 65.00, "In Stock", "seetha_type2", "seetha_type2_s", "seetha_type2_h", "seetha_type2_v");

        productList.add(product);

        product = new Product(8, "Guava Green ", 4, R.drawable.guava_type1, 50.00, 40.00, "In Stock", "guava_type1", "guava_type1_s", "guava_type1_h", "guava_type1_v");

        productList.add(product);

        product = new Product(9, "Guava Red", 4, R.drawable.guava_type2, 90.00, 70.00, "In Stock", "guava_type2", "guava_type2_s", "guava_type2_h", "guava_type2_v");

        productList.add(product);

        product = new Product(10, "Travel bag", 5, R.drawable.travel_type1, 195.00, 170.00, "In Stock", "travel_type1", "travel_type1_s", "travel_type1_h", "travel_type1_v");

        productList.add(product);
        product = new Product(11, "Travel bag big", 5, R.drawable.travel_type2, 200.00, 170.00, "In Stock", "travel_type2", "travel_type2_s", "travel_type2_h", "travel_type2_v");

        productList.add(product);
        product = new Product(12, "Baniyan black", 7, R.drawable.shirt_type1, 200.00, 170.00, "In Stock", "shirt_type1", "shirt_type1_s", "shirt_type1_h", "shirt_type1_v");

        productList.add(product);
        product = new Product(13, "Baniyan grey", 7, R.drawable.shirt_type2, 200.00, 175.00, "In Stock", "shirt_type2", "shirt_type2_s", "shirt_type2_h", "shirt_type2_v");

        productList.add(product);

        product = new Product(14, "Baniyan white", 7, R.drawable.shirt_type3, 205.00, 175.00, "In Stock", "shirt_type3", "shirt_type3_s", "shirt_type3_h", "shirt_type3_v");

        productList.add(product);
        product = new Product(15, "Checked Shirt1 ", 8, R.drawable.shirt_checked2, 700.00, 550.00, "In Stock", "shirt_checked2", "shirt_checked2_s", "shirt_checked2_h", "shirt_checked2_v");

        productList.add(product);

        product = new Product(16, "Checked Shirt2  ", 8, R.drawable.shirt_checked3, 500.00, 450.00, "In Stock", "shirt_checked3", "shirt_checked3_s", "shirt_checked3_h", "shirt_checked3_v");

        productList.add(product);


        product = new Product(17, "Pant type 1 ", 9, R.drawable.pant_type1, 500.00, 470.00, "In Stock", "pant_type1", "pant_type1_s", "pant_type1_h", "pant_type1_v");

        productList.add(product);

        product = new Product(18, "Pant type 2", 9, R.drawable.pant_type2, 600.00, 560.00, "In Stock", "pant_type2", "pant_type2_s", "pant_type2_h", "pant_type2_v");

        productList.add(product);


        product = new Product(19, "Jean pant 1", 10, R.drawable.pant_jean1, 1100.00, 1070.00, "In Stock", "pant_jean1", "pant_jean1_s", "pant_jean1_h", "pant_jean1_v");

        productList.add(product);

        product = new Product(20, "Jean pant 2", 10, R.drawable.pant_jean2, 1000.00, 970.00, "In Stock", "pant_jean2", "pant_jean2_s", "pant_jean2_h", "pant_jean2_v");

        productList.add(product);


        product = new Product(21, "Bureau wood1", 11, R.drawable.bureau_type1, 11100.00, 10000.00, "In Stock", "bureau_type1", "bureau_type1_s", "bureau_type1_h", "bureau_type1_v");

        productList.add(product);
        product = new Product(22, "Bureau wood2", 11, R.drawable.bureau_type2, 10100.00, 9000.00, "In Stock", "bureau_type2", "bureau_type2_s", "bureau_type2_h", "bureau_type2_v");

        productList.add(product);
        product = new Product(23, "Clock small ", 13, R.drawable.clock_type1, 300.00, 270.00, "In Stock", "clock_type1", "clock_type1_s", "clock_type1_h", "clock_type1_v");

        productList.add(product);
        product = new Product(24, "Clock", 13, R.drawable.clock_type2, 300.00, 250.00, "In Stock", "clock_type2", "clock_type2_s", "clock_type2_h", "clock_type2_v");

        productList.add(product);


        product = new Product(25, "Lamp set 1", 15, R.drawable.lamp_type1, 300.00, 270.00, "In Stock", "lamp_type1", "lamp_type1_s", "lamp_type1_h", "lamp_type1_v");

        productList.add(product);
        product = new Product(26, "Lamp set 2", 15, R.drawable.lamp_type2, 350.00, 250.00, "In Stock", "lamp_type2", "lamp_type2_s", "lamp_type2_h", "lamp_type2_v");

        productList.add(product);


        ProductTableQry tableQry = new ProductTableQry();
        tableQry.insertProductDB(productList, MainActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private void setOnClickListener() {

        cart_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Prefs.getInt(Constants.customer_id, 0) != 0) {
                    Intent in = new Intent(MainActivity.this, CartPage.class);
                    startActivity(in);
                } else {
                    Intent in = new Intent(MainActivity.this, Login.class);
                    startActivity(in);
                }

            }
        });
        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!s.equals("")) {
                    search_layout.setVisibility(View.VISIBLE);
                    main_layout.setVisibility(View.GONE);
                    ProductTableQry dbQuery = new ProductTableQry();
                    final ArrayList<Product> products = dbQuery.searchProduct(s.toString(), MainActivity.this);

                    pdtAdapter = new ProductAdapter(products, new ProductAdapter.OnItemClickListener() {
                        @Override
                        public void OnItemClick(int var1, int position) {
                            if (var1 == R.id.product_row_layout) {
                                Product product = products.get(position);
                                Intent in = new Intent(MainActivity.this, ProductDescription.class);
                                in.putExtra("productDetail", (Serializable) product);
                                startActivity(in);
                            }


                        }
                    }, MainActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                    search_recycler.setLayoutManager(layoutManager);
                    search_recycler.setAdapter(pdtAdapter);
                } else {
                    main_layout.setVisibility(View.VISIBLE);
                    search_layout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setNavigationDrawer() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.my_profile) {
                    // Handle the myprofile  action
                    Intent in = new Intent(MainActivity.this, MyProfile.class);
                    startActivity(in);
                } else if (id == R.id.my_orders) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, MyOrders.class);
                    startActivity(in);
                } else if (id == R.id.feedback) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, Feedback.class);
                    startActivity(in);
                } else if (id == R.id.about_us) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, AboutUs.class);
                    startActivity(in);
                } else if (id == R.id.contact_us) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, ContactUs.class);
                    startActivity(in);
                } else if (id == R.id.order_status) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, OrderStatus.class);
                    startActivity(in);
                } else if (id == R.id.terms_conditions) {
                    // Handle the About action
                    Intent in = new Intent(MainActivity.this, TermsAndConditions.class);
                    startActivity(in);
                } else if (id == R.id.logout) {
                    Prefs.putInt(Constants.customer_id, 0);
                    Toast.makeText(MainActivity.this, "Logged Out Successfully", Toast.LENGTH_LONG).show();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void setBanner() {
        int arrayImages[] = {R.drawable.banner_one, R.drawable.banner_two, R.drawable.banner_three};
        for (int idx = 0; idx < arrayImages.length; idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(this);
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(arrayImages[idx])
                    .setScaleType(BaseSliderView.ScaleType.Fit);


            //add your extra information
            defaultSliderView.bundle(new Bundle());


            slider_layout.addSlider(defaultSliderView);
        }
        slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        slider_layout.setDuration(4000);

    }

    private void prepareCategoryList() {
        Category category = new Category("Vegetable", 1, R.drawable.veg);
        categoryList.add(category);

        category = new Category("Fruit", 2, R.drawable.fruit);
        categoryList.add(category);

        category = new Category("Bags", 3, R.drawable.bag);
        categoryList.add(category);

        category = new Category("Shirts", 4, R.drawable.shirt);
        categoryList.add(category);

        category = new Category("Pants", 5, R.drawable.pant);
        categoryList.add(category);

        category = new Category("Furniture", 6, R.drawable.chair);
        categoryList.add(category);

        category = new Category("Home Decor", 7, R.drawable.homedecor);
        categoryList.add(category);

        category = new Category("Lamps", 8, R.drawable.lamps_lightings);
        categoryList.add(category);
        //Insert category items in local table
        CategoryTableQry db = new CategoryTableQry();
        db.insertIntoCategory(categoryList, MainActivity.this);
        adapter.notifyDataSetChanged();
    }

    private void intializeVariable() {
        slider_layout = (SliderLayout) findViewById(R.id.slider_layout);
        recyclerView = (RecyclerView) findViewById(R.id.category_recyclerview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        cart_img = (ImageView) toolbar.findViewById(R.id.cart_img);
        search_edt = (EditText) toolbar.findViewById(R.id.search_edt);
        search_layout = (RelativeLayout) findViewById(R.id.search_layout);
        main_layout = (RelativeLayout) findViewById(R.id.main_page_layout);
        search_recycler = (RecyclerView) findViewById(R.id.search_recycler);
        cart_total_qty = (TextView) findViewById(R.id.cart_total_qty);
        //setSupportActionBar(toolbar);
    }


}
