package com.jireh.shoppingcart.Activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.shoppingcart.R;
import com.test.jireh.jireh_lib.Adapter.SubCategoryAdapter;
import com.test.jireh.jireh_lib.DB.CategoryTableQry;
import com.test.jireh.jireh_lib.DB.SubCategoryTableQry;
import com.test.jireh.jireh_lib.Model.Category;

import java.util.ArrayList;


/**
 * Created by Muthamizhan C on 18-07-2017.
 */

public class SubCategoryList extends Activity {
    RecyclerView subcategoryRecycler;
    SubCategoryAdapter adapter;
    ArrayList<Category> subcategoryList = new ArrayList();
    ImageView back_img;
    TextView title_text;
    int categoryId;
    String categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subcat_layout);
        initializeVariable();
        setOnClickListener();

        //getCategoryId
        categoryId = getIntent().getIntExtra("categoryId", 0);
        categoryName = getIntent().getStringExtra("categoryTitle");

        title_text.setText(categoryName);
        //set the adapter
        adapter = new SubCategoryAdapter(subcategoryList, new SubCategoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                //  Toast.makeText(MainActivity.this,"Position Clicked" + position,Toast.LENGTH_LONG).show();

                Intent in = new Intent(SubCategoryList.this, ProductList.class);
                in.putExtra("subCategoryId", subcategoryList.get(position).getSub_category_id());
                in.putExtra("subCategoryName", categoryName + "-" + subcategoryList.get(position).getCategory_name());
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        GridLayoutManager manager = new GridLayoutManager(SubCategoryList.this, 2);
        subcategoryRecycler.setLayoutManager(manager);
        subcategoryRecycler.setAdapter(adapter);
        prepareSubCategoryList();
    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void prepareSubCategoryList() {
        if (categoryId == 1) {
            Category category = new Category(1,"Carrot", 1, R.drawable.carrot);
            subcategoryList.add(category);
            category = new Category(1,"Onion", 2, R.drawable.onion);
            subcategoryList.add(category);

        } else if (categoryId == 2) {
            Category category = new Category(2,"Caribbean", 3, R.drawable.seetha);
            subcategoryList.add(category);
            category = new Category(2,"Guava", 4, R.drawable.guava);
            subcategoryList.add(category);

        } else if (categoryId == 3) {
            Category category = new Category(3,"Travel bag", 5, R.drawable.travel_1);
            subcategoryList.add(category);
            category = new Category(3,"Office Bag", 6, R.drawable.travel_2);
            subcategoryList.add(category);
        } else if (categoryId == 4) {
            Category category = new Category(4,"Baniyan", 7, R.drawable.shirt_1);
            subcategoryList.add(category);
            category = new Category(4,"T Shirt", 8, R.drawable.shirt_checked1);
            subcategoryList.add(category);
        } else if (categoryId == 5) {
            Category category = new Category(5,"Pant", 9, R.drawable.pant_1);
            subcategoryList.add(category);
            category = new Category(5,"Jeans pant", 10, R.drawable.pant_2);
            subcategoryList.add(category);
        } else if (categoryId == 6) {
            Category category = new Category(6,"Table", 11, R.drawable.bureau);
            subcategoryList.add(category);
            category = new Category(6,"Book Stand", 12, R.drawable.library);
            subcategoryList.add(category);
        } else if (categoryId == 7) {
            Category category = new Category(7,"Clock Set", 13, R.drawable.clock);
            subcategoryList.add(category);
            category = new Category(7,"Pillow set", 14, R.drawable.pillow);
            subcategoryList.add(category);
        } else if (categoryId == 8) {
            Category category = new Category(8,"Lamp Set ", 15, R.drawable.new_lamp);
            subcategoryList.add(category);
            category = new Category(8,"Lamp ", 16, R.drawable.lamps);
            subcategoryList.add(category);
        }
        //Insert category items in local table
        SubCategoryTableQry db = new SubCategoryTableQry();
        db.insertIntoSubCategory(subcategoryList, SubCategoryList.this);
        adapter.notifyDataSetChanged();
    }

    private void initializeVariable() {
        subcategoryRecycler = (RecyclerView) findViewById(R.id.subcategorylist);
        title_text = (TextView) findViewById(R.id.title_text);
        back_img = (ImageView) findViewById(R.id.back_img);


    }
}
